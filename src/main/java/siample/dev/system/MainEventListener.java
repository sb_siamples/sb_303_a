package siample.dev.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext; // ne legyen: Catalina.Core
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import siample.dev.dao.InitDAO;

@Component
public class MainEventListener {

	@Autowired
	ApplicationContext applicationContext;
	@Autowired
	InitDAO initDAO;

	/**
	 * A MainEventListener belseje akkor fut, amikor épp indul az alkalmazás de a
	 * tomcat még nem indult el. Addig user-ek még nem hívhatják az alkalmazást, és
	 * itt alakíthatok ki egy egész teljes DB struktúrát, ami akár 1-2 percig is fut.
	 * A tomcat még nem indult el, vagyis nem lehet böngészőből hívni az alkalmazást, 
	 * hiszen még a DB felépítése folyik, és a userek még ki vannak tiltva.
	 */
	@EventListener
	public void onApplicationEvent(ContextRefreshedEvent event) {  // ContextRefreshedEvent raised when
	                                         	                   // an ApplicationContext gets initialized or refreshed!
		initDAO.strukturaLetrehozas();
	}
}
