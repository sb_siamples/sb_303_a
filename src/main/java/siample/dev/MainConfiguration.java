package siample.dev;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@Configuration
public class MainConfiguration {

	/**
	 * app.yml-ból vett értékekkel feltöltött Datasource a H2 memory adatbázishoz
	 */
	@Bean
	@ConfigurationProperties("app.db")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}
	
	/**
	 * NamedParameterJdbcTemplate (tehát nem '?' paraméterezett sql-ek) a H2 memory adatbázishoz
	 */
	@Bean
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate(@Autowired DataSource dataSource) {
		return new NamedParameterJdbcTemplate(dataSource);
	}
	
}
